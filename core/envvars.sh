#!/bin/bash

# Mimic editor to edit aliases
MIMIC_EDITOR=""

# Mimic root dir
MIMIC_HOME_DIR="$(realpath $0)"
MIMIC_HOME_DIR="$(dirname ${MIMIC_HOME_DIR})"
MIMIC_HOME_DIR=$(dirname "${MIMIC_HOME_DIR}" | sed "s|~|${HOME}|")

# Mimic dependencies dir
MIMIC_CORE_DIR="${MIMIC_HOME_DIR}/core"

# Mimic shared dir. Could contain functions that are shared across mimic libraries
MIMIC_SHRD_DIR="${MIMIC_HOME_DIR}/shared"

# Mimic binary dir
MIMIC_BIN_DIR="${MIMIC_HOME_DIR}/bin"

# Mimic aliases base dir
MIMIC_ALIAS_DIR="${MIMIC_HOME_DIR}/aliases"

# Mimic templates dir
MIMIC_TMPL_DIR="${MIMIC_CORE_DIR}/template"

# Mimic defaults dir
MIMIC_DFLT_DIR="${MIMIC_CORE_DIR}/default"

# Mimic editor definition file
MIMIC_EDITOR_FILE="$MIMIC_CORE_DIR/mimic-editor.txt"

# Mimic editor
MIMIC_EDITOR="$(cat $MIMIC_CORE_DIR/mimic-editor.txt | sed 's/\n//g' )"

# Mimic current alias name
MIMIC_CURR_ALIAS="${1}"

# Name of the binary to mimic. eg: git, netstat, ps, npx, etc
MIMIC_CURR_BIN="$(basename $0)"

# Mimic current alias dir
MIMIC_CURR_ALIAS_DIR="${MIMIC_ALIAS_DIR}/${MIMIC_CURR_BIN}"