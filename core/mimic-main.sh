#!/bin/bash

#----------------------------------------------------
# Load env vars
. "$(dirname $0)/../core/envvars.sh"
#----------------------------------------------------

#----------------------------------------------------
# Load shared functions
for shrdFl in `find "${MIMIC_SHRD_DIR}" -type f -name "*.sh"`
do
  [ -f "${shrdFl}" ] && . ${shrdFl}
done
#----------------------------------------------------

#----------------------------------------------------
# Load default mimic handlers
. "${MIMIC_DFLT_DIR}/--mimic-generic.sh"
#---------------------------------------------------

if [ "${MIMIC_CURR_ALIAS}" == "!" ];then # Invoke original binary directly. Skip any aliases
    shift # Because first argument is [!]
    CALLER_NAME="${MIMIC_ORIGINAL_BIN}"
    # Send everything to the original binary
    . ${MIMIC_CORE_DIR}/mimic-invoke.sh
    return $?
elif [ "${MIMIC_CURR_ALIAS}" == "--" && -s "${MIMIC_ALIAS_DIR}/${MIMIC_PROXY_ALIAS}" ];then # Invoke default user alias
    shift # Because first argument is [--]
    . "${MIMIC_ALIAS_DIR}/${MIMIC_PROXY_ALIAS}"
    CALLER_NAME="alias_do"
    . ${MIMIC_CORE_DIR}/mimic-caller.sh
    return $?
elif [ -s "${MIMIC_ALIAS_DIR}/${MIMIC_CURR_ALIAS}" ];then # Invoke user defined alias
    shift # Because first argument is [${MIMIC_CURR_ALIAS}]
    als_file=$(resolve_path ${MIMIC_ALIAS_DIR}/${MIMIC_CURR_ALIAS}})
    . "$als_file"
    MIMIC_ALIAS="$(basename $als_file)"
    CALLER_NAME="alias_do"
    . ${MIMIC_CORE_DIR}/mimic-caller.sh
    return $?
elif [ -s "${MIMIC_DFLT_DIR}/${MIMIC_CURR_ALIAS}.sh" ];then # Invoke extra functionality action[C.R.U.D./EXPORT/IMPORT]
    shift # Because it's functionality call[C.R.U.D./EXPORT/IMPORT]
    . ${MIMIC_DFLT_DIR}/${MIMIC_ALIAS}.sh
    als_file="$(resolve_path ${MIMIC_DFLT_DIR}/${MIMIC_ALIAS}.sh)"
    . "$als_file"
    MIMIC_ALIAS="$(basename $als_file)"
    CALLER_NAME="alias_do"
    . ${MIMIC_CORE_DIR}/mimic-caller.sh
    return $?
elif [ -s "${MIMIC_ALIAS_DIR}/${MIMIC_PROXY_ALIAS}" ];then # Invoke proxy if defined
    . "${MIMIC_ALIAS_DIR}/${MIMIC_PROXY_ALIAS}"
    MIMIC_ALIAS="$(basename $1)"
    shift # Because first argument is [${MIMIC_CURR_ALIAS}]
    CALLER_NAME="alias_do"
    . "${MIMIC_CORE_DIR}/mimic-invoke.sh"
    return $?
elif ! [ -z "${MIMIC_BIN}" ];then
  #echo "Call empty original bin ${MIMIC_BIN} $*"
  CALLER_NAME="${MIMIC_BIN}"
  . ${MIMIC_CORE_DIR}/mimic-invoke.sh
  return $?
fi

#echo "${MIMIC_DFLT_DIR}/${MIMIC_ALIAS}.sh"
#[ -f "${MIMIC_DFLT_DIR}/${MIMIC_ALIAS}.sh" ] && echo "EXISTS"

echo "Something went wrong while processing alias ${MIMIC_CALLER} ${MIMIC_ALIAS}"
echo "Check alias contents by hitting"
echo "${MIMIC_CALLER} --mimic-edit ${MIMIC_ALIAS}"

###alias_help $*
###alias_do $*
return 22

#compgen -f help_line
